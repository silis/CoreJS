/*(c)http://gitee.com/silis/CoreJS*/

(function(win){

	if(!win.document.addEventListener){
		win.document.addEventListener = function(event, completed){
			switch(event){
				case "DOMContentLoaded":
					win.document.attachEvent( "onreadystatechange", completed );
					break;
			}
		}

		win.document.removeEventListener = function(event, completed){
			switch(event){
				case "DOMContentLoaded":
					win.document.detachEvent( "onreadystatechange", completed );
					break;
			}
		}
	}

	if(!win.document.getElementsByClassName){
		win.document.getElementsByClassName = function(className){
			var elements = [],
				regexp = new RegExp('(\\s|^)(?:'+className.trim()+')(?=\\s|$)');

			Array.prototype.forEach.call(win.document.getElementsByTagName('*'), function(element){
				if(regexp.test(element.className)){
					elements.push(element)
				}
			});

			return elements;
		}
	}

    win.document.parse = function(input)
    {
        var parseElementQueue = [win.document.createDocumentFragment()];
        (input||'').replace(/<(\/?)([_\-\w]+)([^>]*)(\/?)>|([^<]*)/g, function(t0, tagStart, tagName, tagAttrs, tagEnd, text){
            if(tagName != null && tagName.length > 0){
                if(tagStart == "")
                {
                    var element = win.document.createElement(tagName);
                    parseElementQueue[parseElementQueue.length-1].appendChild(element);

                    tagAttrs.replace(/([_\-\w]+)\s*=\s*(['][^']*[']|["][^"]*["])/g, function(a0,a1,a2){
                        element.setAttribute(a1,a2);
                        return "";
                    });

                    if(tagEnd == "") parseElementQueue.push(element);
                }
                else
                {
                    parseElementQueue.pop();
                }
            } else {
                parseElementQueue[parseElementQueue.length-1].appendChild(win.document.createTextNode(text))
            }

            return "";
        })

        return parseElementQueue[0];
    }


})(window);